import express from 'express';
import fetch from 'isomorphic-fetch';
import Promise from 'bluebird';
import bodyParser from 'body-parser';
import _ from 'lodash';

const __DEV__ = true;

const app = express();
app.use(bodyParser.json());

app.use(function(req, res, next) {
  res.header("Access-Control-Allow-Origin", "*");
  res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");
  next();
});

function rgb2hex(rgb){
  const tmpRgb = rgb.match(/^rgba?[\s+]?\([\s+]?(\d+)[\s+]?,[\s+]?(\d+)[\s+]?,[\s+]?(\d+)[\s+]?\)$/i);
  if (!tmpRgb) return '';
  const num1 = parseInt(tmpRgb[1], 10);
  const num2 = parseInt(tmpRgb[2], 10);
  const num3 = parseInt(tmpRgb[3], 10);
  if (isNaN(num1) || num1 > 255 || isNaN(num2) || num2 > 255 || isNaN(num3) || num3 > 255) return '';
  return (tmpRgb && tmpRgb.length === 4) ?
  ("0" + num1.toString(16)).slice(-2) +
  ("0" + num2.toString(16)).slice(-2) +
  ("0" + num3.toString(16)).slice(-2) : '';
}

function hslToRgb(h, s, l) {
    var r, g, b;
    if(s == 0){
        r = g = b = l; // achromatic
    }else{
        var hue2rgb = function hue2rgb(p, q, t){
            if(t < 0) t += 1;
            if(t > 1) t -= 1;
            if(t < 1/6) return p + (q - p) * 6 * t;
            if(t < 1/2) return q;
            if(t < 2/3) return p + (q - p) * (2/3 - t) * 6;
            return p;
        }
        var q = l < 0.5 ? l * (1 + s) : l + s - l * s;
        var p = 2 * l - q;
        r = hue2rgb(p, q, h + 1/3);
        g = hue2rgb(p, q, h);
        b = hue2rgb(p, q, h - 1/3);
    }
    return [Math.round(r * 255), Math.round(g * 255), Math.round(b * 255)];
}


function hsl2hex(hsl) {
  const tmpHsl = hsl.match(/^hsl?[\s+]?\([\s+]?(\d+)[\s+]?,[\s+]?(\d+%)[\s+]?,[\s+]?(\d+%)[\s+]?\)$/i);
  if (!tmpHsl) return '';
  let num1 = parseInt(tmpHsl[1], 10) / 360;
  let num2 = parseInt(tmpHsl[2].substring(0, tmpHsl[2].length - 1), 10) / 100;
  let num3 = parseInt(tmpHsl[3].substring(0, tmpHsl[3].length - 1), 10) / 100;
  if (isNaN(num1) || num1 > 1 || isNaN(num2) || num2 > 1 || isNaN(num3) || num3 > 1) return '';
  const tmpRgb = hslToRgb(num1, num2, num3);
  num1 = tmpRgb[0];
  num2 = tmpRgb[1];
  num3 = tmpRgb[2];
  if (isNaN(num1) || num1 > 255 || isNaN(num2) || num2 > 255 || isNaN(num3) || num3 > 255) return '';
   console.log('hsl2hex output');
  return ("0" + num1.toString(16)).slice(-2) + ("0" + num2.toString(16)).slice(-2) + ("0" + num3.toString(16)).slice(-2);
}

app.get('/task2D/*', (req, res, next) => {
  let data = req.query.color;
  console.log('Requested: ' + data);
  const regexp1 = new RegExp('[\\dabcdefABCDEF]{3}', 'g');
  const regexp2 = new RegExp('[\\dabcdefABCDEF]{6}', 'g');
  let tmp = false;

  if (!data) res.send('Invalid color')
  data = unescape(data.trim()).replace(/\s/gi,'').toLowerCase();

 console.log('Testing: ' + data);
  if (data.startsWith('#')) {
    data = data.substring(1);
  } else if (data.startsWith('rgb')) {
    data = rgb2hex(data);
  } else if (data.startsWith('hsl')) {
    data = hsl2hex(data);
  }

  console.log('Output: ' + data);
  if (data.length === 3) {
    tmp = regexp1.test(data);
    let tempData = '';
    for (let i = 0; i < data.length; i++) {
      tempData = tempData + data[i] + data[i];
    }
    data = tempData;
  } else if (data.length === 6) {
    tmp = regexp2.test(data);
  }

  if (tmp) res.send('#' + data);
  else res.send('Invalid color');

  return;
});

app.listen(3000, () => {
  console.log('Example app listening on port 3000!');
});
